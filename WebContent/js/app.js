/**
 *  Street Fighter js
 */

var isHover = false;
//Load Javascript
$(document).ready(function() {
	
	$("#playHadouken")[0].load();
	$("#pose-sound")[0].load();
	
	//show ryu-ready on mouseenter
	$(".ryu").mouseenter(function() {
		$(".ryu-still").hide();
		$(".ryu-ready").show();
		isHover = true;
	}).mouseleave(function() {
		//Hide ryu-ready on mouseleave
		$(".ryu-ready").hide();
		$(".ryu-still").show();
		isHover = false;
	}).mousedown(function() {
		playHadouken(); //Play hadouken-sound on mousedown
		//show throwing, hide ready
		$(".ryu-ready").hide();
		$(".ryu-throwing").show();
		$(".hadouken").show().animate(
			{left: "+=100px"}, 280, // 280: ms length of animation
			function() {
				$(this).hide();
				$(this).css("left","31rem");
			})
	}).mouseup(function() {
		$(".ryu-throwing").hide();
	}).mouseleave(function() {
		$(".ryu-throwing").hide();
	})
})
	

	
	//Animate hadouken gif 
	
	//mouseup - hide throwing, show ready 
	
//---
//document ready keydown 'x'
$(document).keydown(function(evtkey){
	if ( evtkey.keyCode == 88 ) {
		playPose();
		$(".ryu-ready").hide();
		$(".ryu-throwing").hide();
		$(".ryu-still").hide();
		$(".ryu-cool").show();
	}
}).keyup(function(evtkey) {
	if ( evtkey.keyCode == 88 ) {
		$('#pose-sound')[0].pause();
		$(".ryu-cool").hide();
		if ( isHover == true ) {
			$(".ryu-ready").show();
		} else {
			$(".ryu-still").show();
		}
	}
})
//playPose, show cool, hide still, hide ready
var playSound = false;
function playPose() {
	playSound = !playSound; // toggle
	if ( playSound ) {
		$("#pose-sound")[0].volume = 0.2;
		//$("#pose-sound")[0].load();
		$("#pose-sound")[0].play();
	}
}
//keyup 'x' pose-sound pause/load, hide cool
//if hover show ready else show still
//---


//Play hadouken fn - volume, load, play
function playHadouken() {
	$("#playHadouken")[0].volume = 0.2;
	//$("#playHadouken")[0].load();
	$("#playHadouken")[0].play();
}
//Play prose fn 